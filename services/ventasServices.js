
const mysql = require("mysql");

const conn = require("../config/conn");

const ventas = {
  async postVentas(nuevo) {
    let sql =
      "INSERT INTO `ecommerce`.`ventas` ( `id` , `cliente_id`, `total`)VALUES (NULL , '" +
      nuevo.cliente_id +
      "', '" +
      nuevo.total +
      "'); SELECT LAST_INSERT_ID() AS id;";
   ;
    let resultado = await conn.query(sql);
    let response = { mensaje: "venta generada" };
    let ventaId = resultado[0].insertId;
    if (resultado.code) {
      response = { mensaje: "Error en la consulta SQL44" } 
       } else if (resultado.length > 0) {
      let sql =
        "INSERT INTO `detalles_ventas`( `venta_id`, `producto_id`, `cantidad`, `precio`, `subtotal`) VALUES ";
      let coma = "";
      for (let index = 0; index < nuevo.productos.length; index++) {
        let consulta =
          "(" +
          ventaId +
          "," +
          nuevo.productos[index].id +
          "," +
          nuevo.productos[index].cantidad +
          "," +
          nuevo.productos[index].precio +
          ",244)";

        sql = sql + coma + consulta;
        coma = ",";
      }     
      let resultadoDos = await conn.query(sql);
      response = { result: resultadoDos };
    }
    return response;
  },
 
  async getVentas() {  
    let sql = "SELECT * FROM ventas";  
    let resultado = await conn.query(sql);
    let response = { mensaje: "No se encontraron registros" };
    if (resultado.code) {
      response = { mensaje: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getVentasById(id) {
    let sql = "SELECT * FROM ventas WHERE id = " + id;
    let resultado = await conn.query(sql);
    let response = { mensaje: "No se encontraron registros" };
    if (resultado.code) {
      response = { mensaje: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getDettalesVentas(id) {
    let sql =
      "SELECT `id`, `venta_id`, `producto_id`, `cantidad`, `precio`, `subtotal` FROM `detalles_ventas` WHERE 1 WHERE id =" +
      id;
  },
};

module.exports = ventas;
