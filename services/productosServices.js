
const mysql = require("mysql");

const conn = require("../config/conn");


const productos = {
  async getProductos() {    
    let sql = "SELECT * FROM productos  WHERE `visible_venta` <  `stock` ";
        let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getByIdProductos(id) {  
    let sql = "SELECT * FROM rol WHERE id =" + id;
      let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    } else if (resultado.length === 0) {
      response = { result: "no hay registro con este ID " };  
    }
    return response;
  },
  async postProductos(nuevo) {    
    let sql =
      'INSERT INTO `productos`(`id`, `nombre`, `precio`, `stock`, `categoria_id`) VALUES (NULL,"' +
      nuevo.nombre +
      '","' +
      nuevo.precio +
      '","' +
      nuevo.stock +
      '","' +
      nuevo.categoria_id +
      '")';   
    let resultado = await conn.query(sql);
    let response = { error: "se creo producto con exito" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async putProductos(nuevo) {    
    let sql =
      "UPDATE `productos` SET `nombre`='" +
      nuevo.nombre +
      "', `precio`=" +
      nuevo.precio +
      ", `stock`=" +
      nuevo.stock +
      ", `categoria_id`=" +
      nuevo.categoria_id +
      " WHERE `id`=" +
      nuevo.id;    
    let resultado = await conn.query(sql);
    let response = { error: "se modifico el producto" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL de put" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async deleteProductos(id) {
  
    let sql =
      "UPDATE `productos` SET `visible_venta` = 1 WHERE `id` AND stock < 5";
      let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};
module.exports = productos;
