const mysql = require("mysql");

const conn = require("../config/conn");

const entrada_mercaderia = {
  async getEntrada_mercaderia() {
    let sql = "SELECT * FROM entrada_mercaderia  WHERE `costo_activo` = '0'";
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getByIdEntrada_mercaderia(id) {
    let sql = "SELECT * FROM entrada_mercaderia WHERE id =" + id;
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    } else if (resultado.length === 0) {
      response = { result: "no hay registro con este ID " };
    }
    return response;
  },

  async postEntrada_mercaderia(ingreso) {
    let sql =
      'INSERT INTO `entrada_mercaderia`(`id`, `producto_id`, `cantidad`, `costo`) VALUES (NULL,"' +
      ingreso.producto_id +
      '","' +
      ingreso.cantidad +
      '","' +
      ingreso.costo +
      '")';
    let resultado = await conn.query(sql);
    let response = { error: "ingreso de mercaderia con exito" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async putEntrada_mercaderia(modifica) {
    let sql =
      "UPDATE `entrada_mercaderia` SET `producto_id`='" +
      modifica.producto_id +
      "', `cantidad`=" +
      modifica.cantidad +
      ", `costo`=" +
      modifica.costo +
      " WHERE `id`=" +
      modifica.id;

    let resultado = await conn.query(sql);
    let response = { error: "se modifico el ingreso" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL de put" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async deleteEntrada_mercaderia(id) {
   
    let sql =
      "UPDATE `entrada_mercaderia` SET `costo_activo` = 1 WHERE `costo` = 0 ";
       let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};

module.exports = entrada_mercaderia;
