
const mysql = require("mysql");

const conn = require("../config/conn");


const rol = {  
  async getRol() {    
    let sql = "SELECT * FROM rol WHERE `activo` = '0'";    
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros de rol" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getByIdRol(id) {    
    let sql = "SELECT * FROM rol WHERE id =" + id;   
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    } else if (resultado.length === 0) {
      response = { result: "no hay registro con este ID " };  
    }
    return response;
  },

  async postRol(dato) {  
    let sql =
      'INSERT INTO `rol`(`id`, `nombre`) VALUES (NULL,"' + dato.nombre + '")';
        let resultado = await conn.query(sql);
    let response = { error: "se registro dato de rol con exito " };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async putRol(dato) {  
    let sql =
      "UPDATE `rol` SET `nombre`='" +
      dato.nombre +
      "' WHERE `id`= '" +
      dato.id +
      "'";   
    let resultado = await conn.query(sql);
    let response = { error: "se modifico el dato de rol" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL de put" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async deleteRol(id) {   
    let sql = "UPDATE `rol` SET `activo`='1' WHERE `id`= '" + id + "'";    
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};

module.exports = rol;
