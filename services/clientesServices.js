
const mysql = require("mysql");

const conn = require("../config/conn");

const clientes = {
  async getClientes() {
   
    let sql = "SELECT * FROM `clientes`WHERE `activo` = '0'";  
        let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getByIdClientes(id) {
    
    let sql = "SELECT * FROM clientes WHERE`activo`='0' AND id =" + id;
        let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }  else if (resultado.length === 0) {
        response = { result: "no hay registro con este ID " };
    }
    return response;
  },


  async postClientes(ingreso) {
       let sql =
      'INSERT INTO `clientes`(`id`, `nombre`, `email`, `direccion`, `telefono`) VALUES (NULL,"' +
      ingreso.nombre +
      '","' +
      ingreso.email +
      '","' +
      ingreso.direccion +
      '","' +
      ingreso.telefono +
      '")';  
    let resultado = await conn.query(sql);
    let response = { error: "cliente registrado con exito" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async putClientes(modifica) {
       let sql =
      "UPDATE `clientes` SET `nombre`='" +
      modifica.nombre +
      "', `email`='" +
      modifica.email +
      "', `direccion`='" +
      modifica.direccion +
      "', `telefono`='" +
      modifica.telefono +
      "' WHERE `id`= '" +
      modifica.id +
      "'";  
    let resultado = await conn.query(sql);
    let response = { error: "se modificaron datos de cliente" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL de put" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async deleteClientes(id) {   
    let sql = "UPDATE `clientes` SET `activo`='1' WHERE `id`= '" + id + "'";
        let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

};

module.exports = clientes;
